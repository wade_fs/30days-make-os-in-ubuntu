ubuntu230os-cherishsir : https://github.com/cherishsir/ubuntu230os
HariboteOS-tyfkda : https://github.com/tyfkda/haribote
osask-linux : https://github.com/zchrissirhcz/osask-linux
RPiHaribote : https://github.com/moizumi99/RPiHaribote
OSASK: https://github.com/Tassandar/OSASK

##`操作系统的学习开发环境是在ubuntu下进行的`
###所有代码均在HP g4的笔记本上u盘启动验证过

day-0-1: https://segmentfault.com/a/1190000000609485
day-0-2: https://segmentfault.com/a/1190000000609495
day-1-0: https://segmentfault.com/a/1190000000609983
day-1-1: https://segmentfault.com/a/1190000000612815
day-2:   https://blog.csdn.net/cherishsir/article/details/26930053
day-3.0: https://blog.csdn.net/cherishsir/article/details/27247887
day-3.1: https://blog.csdn.net/cherishsir/article/details/27517621
day-3.2: https://blog.csdn.net/cherishsir/article/details/29368655

總結: https://segmentfault.com/a/1190000000609473

30天自製os的學習也告一段落，因為有其它更重要的事情要集中精力去處理，書本從15天開始就是多任務了，但是不得不停下一陣子。

下面总结下学习中遇到的一些问题
學習這前14天中，問題最大的是關於GDT，IDT的理解，還有段的訪問長段，屬性的理解。

這其中遇到的一個現象就是從qemu可以正常啟動，但是寫到u盤後，從u盤啟動時，會不停的重啟，可以猜測到是因為產生了一些異常導致電腦重啟，但是由於水平有限一直不知道為什麼， 後來放下30天這本書，去看了一下，關於GDT的訪問屬性的問題，發現出現問題的原因如下：

段的分頁位沒有開，導致從段基址開始，能訪問的空間只有1MB的範圍，而博主寫的代碼與日本作者的有些不同，跳轉到c語言寫的代碼指令已經超過了2MB

日本作者的代碼在初始化GDT，IDT的函數中，對代碼段的長度進行了控制

在10幾天的代碼中，最難理解理解的還是關於圖層刷新的那一塊，雖然代碼是參考書本上敲的，但是還是有很多不太理解，當然只知道是怎麼用的，似乎是基礎有點薄弱的原因。

之前寫的512字節的代碼是用Intel的彙編語言寫的，第二次學習時，全部用GNU的AT&T語法進行了改寫，通過對u盤啟動的研究，發現了關於U盤的一些CHS的特點，當然這一點為以後學習Linux內核時，修改為從u盤啟動是有幫助的。

學完書本上第14天的內容後，發現之前寫的代碼有很多bug,當然這些bug在後面都給修復了。不停的修復bug，也為後面的學習提供了一些方便。由於前面的學習過程中沒有太注重代碼的可讀性，當然只為了自己能看懂，寫了很多註釋。在13天中，對代碼的函數進行了分模塊的整理，對全局變量的存放位置也有更好的存放位置，代碼的結構也更清楚了

在移植ucgui時，對於靜態鏈接庫的使用，也有更深入的理解，因為需要寫makefile來編譯ucgui，對於makefile文件的編寫也有了更深入的了解，當然學習還是不夠系統，後面應該找本makefile的書，系統的看一遍。

關於不同分辨率的顯示問題，有些問題，難道不能在32位的模式下，用c語言來進行設置嗎？不同的模式，不同的VRAM的地址也不同，所以32位的系統只能用2.6GB 左右的內存，高址的內存有一部分給分配給顯存了。關於高分辨率的第14天的關半部分，日本作者寫的有點問題，直接給出了VRAM的地址是0xe0000000是不科學的，導致博主認為自己的代碼有問題，看了好幾次，發現沒有問題。於是直接往書的後面看，發現VRAM的地址是通過中斷讀到的，然後把讀到的VRAM地址保存起來，以便後面在c語言的函數中使用。


https://s3-ap-southeast-1.amazonaws.com/google.cn/index.html
